package com.example.postgresdatabasetask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostgresDatabaseTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostgresDatabaseTaskApplication.class, args);
    }

}
