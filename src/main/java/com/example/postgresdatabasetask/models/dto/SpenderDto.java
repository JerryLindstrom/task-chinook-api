package com.example.postgresdatabasetask.models.dto;

public class SpenderDto {
    private String firstName;
    private String lastName;
    private double sum;


    public SpenderDto(String firstName, String lastName, double sum) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sum = sum;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
