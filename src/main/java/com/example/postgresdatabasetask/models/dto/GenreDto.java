package com.example.postgresdatabasetask.models.dto;

public class GenreDto {
    private String firstName;
    private String lastName;
    private String genreName;
    private int count;

    public GenreDto(String firstName, String lastName, String genreName, int count) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.genreName = genreName;
        this.count = count;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
