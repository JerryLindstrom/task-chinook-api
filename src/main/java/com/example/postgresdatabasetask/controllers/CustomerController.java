package com.example.postgresdatabasetask.controllers;


import com.example.postgresdatabasetask.data.CustomerRepository;
import com.example.postgresdatabasetask.models.Customer;
import com.example.postgresdatabasetask.models.dto.GenreDto;
import com.example.postgresdatabasetask.models.dto.SpenderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/customers")
public class CustomerController {
    private CustomerRepository customerRepository;


    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping()
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @GetMapping("/id/{id}")
    public Customer getCustomerById(@PathVariable int id) {
        return customerRepository.getCustomerById(id);
    }

    @GetMapping("/name/{firstName}")
    public List<Customer> getCustomerByName(@PathVariable String firstName) {
        return customerRepository.getCustomerByFirstName(firstName);
    }

    @GetMapping("/limit/{limit}/offset/{offset}")
    public List<Customer> getCustomersBySpanAndOffset(@PathVariable int limit, @PathVariable int offset) {
        return customerRepository.getCustomersBySpanAndOffset(limit, offset);
    }

    @GetMapping("/country")
    public Map<Integer, String> getCustomerOriginCountries() { return customerRepository.getCustomerOriginCountries(); }

    @GetMapping("/spenders")
    public Map<Integer, SpenderDto> getHighestSpenders() { return customerRepository.getHighestSpenders(); }

    @GetMapping("/genre/{id}")
    public Map<Integer, GenreDto> mostPopularGenre(@PathVariable int id) { return customerRepository.mostPopularGenre(id); }


    @PostMapping
    public Boolean createCustomer(@RequestBody Customer customer) {
        return customerRepository.createCustomer(customer);
    }

    @PatchMapping("/id/{newName}/{nameToChange}")
    public List<Customer> updateCustomer(@PathVariable String newName, @PathVariable String nameToChange) {
        return customerRepository.updateCustomer(newName, nameToChange);
    }


}
