package com.example.postgresdatabasetask.data;


import com.example.postgresdatabasetask.models.Customer;
import com.example.postgresdatabasetask.models.dto.GenreDto;
import com.example.postgresdatabasetask.models.dto.SpenderDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.*;

@Service
public class CustomerRepository {
    @Value("${spring.datasource.url}")
    private String url;
    private Connection con = null;

    public List<Customer> getAllCustomers() {
        List<Customer> customers = new ArrayList<>();
        StringBuilder sql = new StringBuilder(3);
        sql.append("SELECT customer_id, first_name, last_name, country, postal_code, phone, email");
        sql.append(" FROM customer");
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql.toString())) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Customer getCustomerById(int id) {
        Customer customer = null;
        StringBuilder sql = new StringBuilder(3);
        sql.append("SELECT customer_id, first_name, last_name, country, postal_code, phone, email");
        sql.append(" FROM customer WHERE customer_id = ?");
        try{ con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
             ps.setInt(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customer = new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email"));
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public List<Customer> getCustomerByFirstName(String firstName) {
        List<Customer> customers = new ArrayList<>();
        StringBuilder sql = new StringBuilder(3);
        sql.append("SELECT customer_id, first_name, last_name, country, postal_code, phone, email");
        sql.append(" FROM customer WHERE first_name = ?");
        try{ con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
             ps.setString(1, firstName);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public List<Customer> getCustomersBySpanAndOffset(int limit, int offset) {
        List<Customer> customers = new ArrayList<>();
        StringBuilder sql = new StringBuilder(3);
        sql.append("SELECT customer_id, first_name, last_name, country, postal_code, phone, email");
        sql.append(" FROM customer ORDER BY customer_id LIMIT ? OFFSET ?");
        try{ con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
            ps.setInt(1, limit);
            ps.setInt(2, offset);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customers.add(new Customer(rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Boolean createCustomer(Customer customer) {
        boolean success = false;
        StringBuilder sql = new StringBuilder(3);
        sql.append("INSERT INTO customer(customer_id, first_name, last_name, country, postal_code, phone, email)");
        sql.append("VALUES(?,?,?,?,?,?,?)");
        try{
            con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
            ps.setInt(1, customer.getId());
            ps.setString(2, customer.getFirstName());
            ps.setString(3, customer.getLastName());
            ps.setString(4, customer.getCountry());
            ps.setString(5, customer.getPostalCode());
            ps.setString(6, customer.getPhoneNumber());
            ps.setString(7, customer.getEmail());
            int result = ps.executeUpdate();
            success = (result != 0);
    }
        catch (SQLException e) {
        e.printStackTrace();
    }
        return success;
    }

    public List<Customer> updateCustomer(String newName, String nameToChange) {
        List<Customer> customers = new ArrayList<>();
        try {
            con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement("UPDATE customer SET first_name = ? WHERE first_name = ?");
                ps.setString(1, newName);
                ps.setString(2, nameToChange);

                ps.executeUpdate();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Map<Integer, String> getCustomerOriginCountries() {
        Map<Integer, String> customers = new LinkedHashMap<>();
        StringBuilder sql = new StringBuilder(3);
        sql.append("SELECT country, COUNT(*) AS total FROM customer");
        sql.append(" GROUP BY country ORDER BY total DESC");
        try {
            con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customers.put(
                            rs.getInt("total"),
                            rs.getString("country"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

        public Map<Integer, SpenderDto> getHighestSpenders() {
            Map<Integer, SpenderDto> customers = new LinkedHashMap<>();
            StringBuilder sql = new StringBuilder(5);
            sql.append("SELECT customer.customer_id, first_name, last_name, SUM(invoice.total) FROM customer");
            sql.append(" INNER JOIN invoice ON invoice.customer_id = customer.customer_id");
            sql.append(" GROUP BY customer.customer_id ORDER BY sum DESC");
            try{ con = DriverManager.getConnection(url);
                PreparedStatement ps = con.prepareStatement(sql.toString());
                 try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        customers.put(
                                rs.getInt("customer_id"),
                                new SpenderDto(rs.getString("first_name"),
                                        rs.getString("last_name"),
                                        rs.getDouble("sum")));
                    }
                }
            }catch (SQLException e) {
                e.printStackTrace();
            }
            return customers;
    }

    public Map<Integer, GenreDto> mostPopularGenre(int id) {
        Map<Integer, GenreDto> customers = new LinkedHashMap<>();
        StringBuilder sql = new StringBuilder(10);
        sql.append("SELECT customer.customer_id, customer.first_name, customer.last_name,");
        sql.append(" genre.\"name\" AS genre_name, COUNT(genre.\"name\") FROM customer");
        sql.append(" INNER JOIN invoice ON invoice.customer_id = customer.customer_id");
        sql.append(" INNER JOIN invoice_line ON invoice_line.invoice_id = invoice.invoice_id");
        sql.append(" INNER JOIN track ON track.track_id = invoice_line.track_id");
        sql.append(" INNER JOIN genre ON genre.genre_id = track.genre_id");
        sql.append(" WHERE customer.customer_id = ?");
        sql.append(" GROUP BY genre.\"name\", customer.first_name, customer.last_name, customer.customer_id");
        sql.append(" ORDER BY COUNT DESC FETCH FIRST 1 ROWS WITH TIES");
        try{ con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
                       ps.setInt(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customers.put(
                            rs.getInt("customer_id"),
                            new GenreDto(rs.getString("first_name"),
                                    rs.getString("last_name"),
                                    rs.getString("genre_name"),
                                    rs.getInt("count")));
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }
}
